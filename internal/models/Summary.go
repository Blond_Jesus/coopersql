package models

import (
	"database/sql"
	"errors"
	"fmt"
)

type Summary struct {
	TaskStatus string `json:"taskStatus,omitempty"`
	Count      int    `json:"count"`
}

type SummaryModel struct {
	DB *sql.DB
}

func (m *SummaryModel) Get() (*[]Summary, error) {
	stmt := `
	SElECT COUNT(*) as "Total Tasks", taskStatus
	FROM tasks
	group by taskStatus; 
	`

	rows, err := m.DB.Query(stmt)
	var summarys []Summary
	summary := Summary{}

	for i := 0; rows.Next(); i++ {
		err := rows.Scan(
			&summary.Count,
			&summary.TaskStatus,
		)
		if err != nil {
			break
		}
		summarys = append(summarys, summary)
		fmt.Printf("%+v", summary)
	}

	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, ErrNoRecord
		} else {
			return nil, err
		}
	}

	return &summarys, nil
}
