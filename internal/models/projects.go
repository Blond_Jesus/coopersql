package models

import (
	"database/sql"
	"errors"
	"fmt"
	"time"
)

type Project struct {
	ID          int       `json:"projectID"`
	Name        string    `json:"projectName"`
	DueDate     time.Time `json:"projectDueDate"`
	Priority    string    `json:"projectPriority"`
	Manager     int       `json:"projectManager"`
	Status      string    `json:"projectStatus"`
	Description string    `json:"projectDescription"`
}

type ProjectModel struct {
	DB *sql.DB
}

func (m *ProjectModel) Get(id int) (*Project, error) {
	stmt := `
	SELECT projectID, projectName, projectDueDate, projectPriority, projectManager, projectStatus, projectDescription
	WHERE projectID = ?
	FROM oraclepm.projects;
	`

	row := m.DB.QueryRow(stmt, id)
	project := &Project{}

	err := row.Scan(
		&project.ID,
		&project.Name,
		&project.DueDate,
		&project.Priority,
		&project.Manager,
		&project.Status,
		&project.Description,
	)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, ErrNoRecord
		} else {
			return nil, err
		}
	}

	return project, nil
}

func (m *ProjectModel) GetAll() (*[]Project, error) {
	stmt := `
	SELECT projectID, projectName, projectDueDate, projectPriority, projectManager, projectStatus, projectDescription
	FROM oraclepm.projects;
	`

	rows, err := m.DB.Query(stmt)
	var projects []Project
	project := Project{}

	for i := 0; rows.Next(); i++ {
		err := rows.Scan(
			&project.ID,
			&project.Name,
			&project.DueDate,
			&project.Priority,
			&project.Manager,
			&project.Status,
			&project.Description,
		)
		if err != nil {
			break
		}
		projects = append(projects, project)
		fmt.Printf("%+v", project)
	}

	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, ErrNoRecord
		} else {
			return nil, err
		}
	}

	return &projects, nil
}

func (m *ProjectModel) Insert(project Project) (int, error) {
	stmt := `
		INSERT INTO oraclepm.projects
		(projectName, projectDueDate, projectPriority, projectManager, projectStatus, projectDescription)
		VALUES(?, ?, ?, ?, ?, ?);
	`
	// duedate, err := time.Parse("2006-01-02", project.DueDate)
	// if err != nil {
	// 	return 0, err
	// }
	result, err := m.DB.Exec(
		stmt,
		project.Name,
		project.DueDate,
		project.Priority,
		project.Manager,
		project.Status,
		project.Description,
	)
	if err != nil {
		return 0, err
	}

	id, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}

	return int(id), nil
}

func (m *ProjectModel) Update(project Project, id int64) error {
	stmt := `
		UPDATE oraclepm.projects
		set projectName = ?, projectDueDate = ?, projectPriority = ?, projectManager = ?, projectStatus = ?, projectDescription = ?
		WHERE projectID = ?
	`

	_, err := m.DB.Exec(
		stmt,
		project.Name,
		project.DueDate,
		project.Priority,
		project.Manager,
		project.Status,
		project.Description,
		id,
	)
	return err
}
func (m *ProjectModel) Delete(id int64) error {
	stmt := `
		DELETE FROM oraclepm.projects
		WHERE projectID = ?
	`

	_, err := m.DB.Exec(
		stmt,
		id,
	)

	return err
}
