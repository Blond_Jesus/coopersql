package models

import (
	"database/sql"
	"errors"
	"fmt"
)

type User struct {
	UserID      int    `json:"userID"`
	UserName    string `json:"userName"`
	LastName    string `json:"lastName"`
	FirstName   string `json:"firstName"`
	PhoneNumber string `json:"phoneNumber"`
	Email       string `json:"uEmail"`
	Access      string `json:"access"`
	Password    string `json:"password"`
}

type UserModel struct {
	DB *sql.DB
}

func (m *UserModel) Get(id int) (*User, error) {
	stmt := `
	SELECT userID, userName, lastName, firstName, phoneNumber, uEmail, Access FROM users
	WHERE userID = ?
	`

	row := m.DB.QueryRow(stmt, id)
	usr := &User{}

	err := row.Scan(
		&usr.UserID,
		&usr.UserName,
		&usr.LastName,
		&usr.FirstName,
		&usr.PhoneNumber,
		&usr.Email,
		&usr.Access,
	)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, ErrNoRecord
		} else {
			return nil, err
		}
	}

	return usr, nil
}

func (m *UserModel) Login(username, password string) (*User, error) {
	stmt := `
	Select userID, firstname, lastname, access
	from oraclepm.users
	where username = ?
	and password = ?
	`

	row := m.DB.QueryRow(stmt, username, password)
	usr := &User{}

	err := row.Scan(
		&usr.UserID,
		&usr.LastName,
		&usr.FirstName,
		&usr.Access,
	)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, ErrNoRecord
		} else {
			return nil, err
		}
	}

	return usr, nil
}

func (m *UserModel) GetAll() (*[]User, error) {
	stmt := `
	SELECT userID, userName, lastName, firstName, phoneNumber, uEmail, Access FROM users
	`

	rows, err := m.DB.Query(stmt)
	var usrs []User
	usr := User{}

	for i := 0; rows.Next(); i++ {
		err = rows.Scan(
			&usr.UserID,
			&usr.UserName,
			&usr.LastName,
			&usr.FirstName,
			&usr.PhoneNumber,
			&usr.Email,
			&usr.Access,
		)
		usrs = append(usrs, usr)
		fmt.Printf("%+v", usr)
	}

	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, ErrNoRecord
		} else {
			return nil, err
		}
	}

	return &usrs, nil
}

func (m *UserModel) Insert(usr User) (int, error) {
	stmt := `
	INSERT INTO users (userName, lastName, firstName, phoneNumber, uEmail, Access, password)
	VALUES(?, ?, ?, ?, ?, ?, ?)
	`

	result, err := m.DB.Exec(
		stmt,
		usr.UserName,
		usr.LastName,
		usr.FirstName,
		usr.PhoneNumber,
		usr.Email,
		usr.Access,
		usr.Password,
	)
	if err != nil {
		return 0, err
	}

	id, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}

	return int(id), nil
}

func (m *UserModel) Update(usr User, id int64) error {
	stmt := `
	UPDATE oraclepm.users
	 set userName = ?, lastName = ?, firstName = ?, phoneNumber = ?, uEmail = ?, access = ?, password = ?
	WHERE userID = ?
	`

	_, err := m.DB.Exec(
		stmt,
		usr.UserName,
		usr.LastName,
		usr.FirstName,
		usr.PhoneNumber,
		usr.Email,
		usr.Access,
		usr.Password,
		id,
	)
	return err
}
func (m *UserModel) Delete(id int64) error {
	stmt := `
		DELETE FROM oraclepm.users
		WHERE userID = ?
	`

	_, err := m.DB.Exec(
		stmt,
		id,
	)

	return err
}
