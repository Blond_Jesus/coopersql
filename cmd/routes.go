package main

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func (app *application) routes() http.Handler {
	router := httprouter.New()

	router.HandlerFunc(http.MethodGet, "/rest/users/:id", app.GetUser)
	router.HandlerFunc(http.MethodGet, "/rest/users", app.GetAllUsers)
	router.HandlerFunc(http.MethodPost, "/rest/users", app.CreateUser)
	router.HandlerFunc(http.MethodDelete, "/rest/users/:id", app.DeleteUser)
	router.HandlerFunc(http.MethodPost, "/rest/users/:id", app.UpdateUser)

	router.HandlerFunc(http.MethodGet, "/user/login/:username/:password", app.UserLogin)

	router.HandlerFunc(http.MethodGet, "/rest/projects/:id", app.GetProject)
	router.HandlerFunc(http.MethodGet, "/rest/projects", app.GetAllProjects)
	router.HandlerFunc(http.MethodPost, "/rest/projects", app.CreateProject)
	router.HandlerFunc(http.MethodDelete, "/rest/projects/:id", app.DeleteProject)
	router.HandlerFunc(http.MethodPost, "/rest/projects/:id", app.UpdateProject)

	router.HandlerFunc(http.MethodGet, "/rest/tasks/:id", app.GetTask)
	router.HandlerFunc(http.MethodGet, "/rest/tasks", app.GetAllTasks)
	router.HandlerFunc(http.MethodPost, "/rest/tasks", app.CreateTask)
	router.HandlerFunc(http.MethodDelete, "/rest/tasks/:id", app.DeleteTask)
	router.HandlerFunc(http.MethodPost, "/rest/tasks/:id", app.UpdateTask)

	router.HandlerFunc(http.MethodGet, "/rest/summary", app.GetSummary)
	return router
}
